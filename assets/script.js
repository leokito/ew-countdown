const navBar = document.createElement('nav')
    navBar.classList.add('navBar')
    document.body.appendChild(navBar)

const container = document.createElement('div')
    container.classList.add('container')
    document.body.appendChild(container)

const link1 = document.createElement('a')
    link1.href = 'https://na.finalfantasyxiv.com/lodestone/'
    link1.innerText = 'Lodestone'
    navBar.appendChild(link1)

const link2 = document.createElement('a')
    link2.href = 'https://pt.wikipedia.org/wiki/Final_Fantasy_XIV'
    link2.innerText = 'Wiki'
    navBar.appendChild(link2)

const link3 = document.createElement('a')
    link3.href = 'https://www.youtube.com/c/ffxiv'
    link3.innerText = 'Youtube'
    navBar.appendChild(link3)

const bannerDiv = document.createElement('div')
    bannerDiv.classList.add('bannerDiv')
    container.appendChild(bannerDiv)

const bannerImage = document.createElement('img')
    bannerImage.src = './assets/img/banner3.png'
    bannerImage.classList.add('bannerImg')
    bannerDiv.appendChild(bannerImage)

const countdown = document.createElement('div')
    countdown.id = 'countdown'
    countdown.classList.add('countdown')
    container.appendChild(countdown)

const cd1 = document.createElement("div")
    cd1.classList.add('cd1')
    cd1.innerText = 'Faltam  ' 
    countdown.appendChild(cd1)

const remainCount = document.createElement('span')
    remainCount.id = 'remainCount'
    countdown.appendChild(remainCount)

const countdownTimer = () => {
    const difference = +new Date("2021-12-03") - +new Date();
    let remaining = "Time's up!";
            
    if (difference > 0) {
    const parts = {
            dias: Math.floor(difference / (1000 * 60 * 60 * 24)),
            // horas: Math.floor((difference / (1000 * 60 * 60)) % 24),
            // minutos: Math.floor((difference / 1000 / 60) % 60),
            // segundos: Math.floor((difference / 1000) % 60),
              };
              remaining = Object.keys(parts).map(part => {
              return `${parts[part]} ${part}`;
              }).join(" ");
            }
        
            document.getElementById("remainCount").innerHTML = remaining
            
        }
            
    countdownTimer();
    // setInterval(countdownTimer, 1000);

const cd2 = document.createElement("div")
    cd2.classList.add('cd1')
    cd2.innerText = ' para o Endwalker'
    countdown.appendChild(cd2)

const btnContainer = document.createElement('div')
    btnContainer.classList.add('btnDiv')
    container.appendChild(btnContainer)

const btnBuy = document.createElement('button')
    btnBuy.classList.add('btn')
    btnBuy.innerText = 'Comprar'
    btnContainer.appendChild(btnBuy)

const modal = document.createElement('div')
        modal.classList.add('modal')
        modal.id = 'modal'
        container.appendChild(modal)

const modalBody = document.createElement('div')
        modalBody.classList.add('modal-body')
        modal.appendChild(modalBody)

const modalTitle = document.createElement('h3')
        modalTitle.innerText = 'Selecione sua Plataforma:'
        modalBody.appendChild(modalTitle)

const steam = document.createElement('div')
    steam.classList.add('steam')
    modalBody.appendChild(steam)
        
const linkSteam = document.createElement('a')
    linkSteam.innerText = 'Steam'
    linkSteam.href = "https://store.steampowered.com/app/1592500/FINAL_FANTASY_XIV_Endwalker/"
    steam.appendChild(linkSteam)

const square = document.createElement('div')
    square.classList.add('square')
    modalBody.appendChild(square)
        
const linkSE = document.createElement('a')
        linkSE.innerText = 'Square Enix Store'
        linkSE.href = "https://store.na.square-enix-games.com/en_US/product/639433/final-fantasy-xiv-endwalker"
        square.appendChild(linkSE)

const psn = document.createElement('div')
        psn.classList.add('psn')
        modalBody.appendChild(psn)
        
const linkPsn = document.createElement('a')
        linkPsn.innerText = 'PSN'
        linkPsn.href = 'https://store.playstation.com/pt-br/category/a3001294-95f0-4ebc-8c26-97f1b4074d47/1'
        psn.appendChild(linkPsn)

btnBuy.addEventListener('click', function() { 
    modal.classList.remove("hidden");
    modal.classList.add("animation-right");
})

// function aparecerModal() {
//     modal.classList.remove("hidden");
//     modal.classList.add("animation-devs-right");
    
// }
function desaparecerModal() {
    let modal = document.getElementById('modal')
    modal.classList.add("hidden");
    modal.classList.remove("animation-right");
}

desaparecerModal()

const trailerDiv = document.createElement('div')
    // trailerDiv.classList.add('trailer')
    trailerDiv.id = 'trailer'
    trailerDiv.style.visibility = 'hidden'
    container.appendChild(trailerDiv)

const tVideo = document.createElement('video')
    tVideo.src = './assets/video/ffxivtrailer.mp4'
    tVideo.controls = 'true'
    trailerDiv.appendChild(tVideo)

const btnTrailer = document.createElement('button')
    btnTrailer.classList.add('btn')
    btnTrailer.innerText = 'Trailer'
    btnContainer.appendChild(btnTrailer)

btnTrailer.addEventListener('click', function() {
        const vid = document.getElementById("trailer");
        const video = document.querySelector('video')
        vid.style.visibility = "visible";
        vid.style.opacity = '1'
        video.play();
        video.currentTime = 0;
    });

bannerImage.addEventListener('click', function() {
    const music = document.getElementById('musica');
    music.volume = 1;
    music.play();
    music.currentTime = 6;
    music.loop = true;
});

const closeImg = document.createElement('img')
    closeImg.classList.add('close')
    closeImg.id ='myElement'
    closeImg.src = './assets/img/close.jpeg'
    closeImg.alt = 'close'
    trailerDiv.appendChild(closeImg)

document.getElementById("myElement").onclick = function() {
        const video = document.querySelector("video")
        // trailer.classList.toggle("active");
        var x = document.getElementById("trailer");
        x.style.visibility = 'hidden'
        x.style.opacity = '0'
        video.pause();
        video.currentTime = 0;
       };

    window.onclick = function(event) {
        if (event.target == modal) {
          modal.classList.add('hidden')
        }
      }


const bgm = document.createElement('audio')
    bgm.id = 'musica'
    bgm.src = './assets/ost/musica.mp3'
    container.appendChild(bgm)
    
    


// const playBgm = () => {
//     const music = document.getElementById('musica');
//     music.volume = 0.02;
//     music.play();
//     music.loop = true;
// }

       // const trailer = document.getElementById('trailer')
       // const video = document.querySelector('video')
       // trailer.classList.toggle("active");
       // trailer.id.toggle('none')
       // video.play();
       // video.currentTime = 0;
